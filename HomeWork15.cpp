﻿
#include <iostream>

void evenOrOdd(bool isEven, int n) {
    for (int i = 0; i <= n; ++i) {
        if ((i % 2 == 0) ^ isEven) {
            std::cout << i << " ";
        }
    }
    std::cout << std::endl;
}

int main()
{
    const int N = 15;

    std::cout << "--Function main--" << std::endl;
    std::cout << "Even numbers: ";
    for (int i = 0; i <= N; ++i) {
        if (i % 2 == 0) {
            std::cout << i << " ";
        }
    }
    std::cout << std::endl << std::endl;

    std::cout << "--Function evenOrOdd--" << std::endl;
    std::cout << "Even numbers: ";
    evenOrOdd(true, N);
    std::cout << "Odd numbers: ";
    evenOrOdd(false, N);
}

